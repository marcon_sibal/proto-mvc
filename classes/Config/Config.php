<?php
namespace Msibal\Config;

use Exception;

/**
 * Config class is a basic configuration helper that read configuration from
 * a configuration file.
 *
 * @todo  Make it multiple config files
 */
class Config
{
    private static $instance;

    private static $config = [];

    private $configFile = 'config/config.php';

    // prevents intanciation
    private function __construct()
    {
        if (file_exists($this->configFile)) {
            self::$config = include($this->configFile);
        }
    }

    /**
     * Reads an array from a file and store it statically
     *
     * @param string|null $configFile: use this to replace the default config file path.
     * @todo  make multiple config file.
     */
    public static function setConfig(string $configFile = null)
    {
        if (isset($configFile)) {
            $this->configFile = $configFile;
        }

        self::$instance = new Config();
    }

    /**
     * Retrieves the value from the configuration.
     *
     * @param  string $configPath [path separated by "."]
     * @return string
     */
    public static function getConfig(string $configPath)
    {
        if (!strrpos($configPath, '.')) {
            if (!isset(self::$config[$configPath])) {
                throw new Exception('Config path "' . $configPath . '" is invalid!');
            }

            return self::$config[$configPath];

        } else {
            $config = self::$config;

            foreach (explode('.', $configPath) as $path) {
                if (!isset($config[$path])) {
                    throw new Exception('Config path "' . $configPath . '" is invalid!');
                }

                $config = $config[$path];
            }

            return $config;
        }

    }
}