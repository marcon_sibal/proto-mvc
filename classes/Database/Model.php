<?php
namespace Msibal\Database;

use Msibal\Database\Database;
use PDO;

abstract class Model
{
    /**
     * Model table name
     * @var string
     */
    protected $table;

    /**
     * Table primary key
     * @var int
     */
    protected $primaryKey = 'id';

    /**
     * PDO connection instance
     */
    protected $pdo;

    /**
     * Store the fields of the table in array for reference when inserting
     * updating.
     *
     * @var array
     */
    protected $fields;

    /**
     * On instantiation do the following
     * - Assign the PDO connection
     * - Set table name if not supplied
     * - Set table fields as properties
     */
    public function __construct()
    {
        $this->pdo = Database::$pdo;

        if (is_null($this->table)) {
                $nsArray = explode("\\", static::class);
                $this->table = str_replace('model', '', strtolower(end($nsArray))) . 's';
        }

        $q = $this->pdo->query('DESCRIBE ' . $this->table);
        $tFields = $q->fetchAll(PDO::FETCH_COLUMN);

        foreach ($tFields as $k) {
            $this->fields[$k] = '';
            $this->{$k} = null;
        }
    }

    /**
     * Static method that finds a record from the db and
     * sets the values of the column to its respective property
     *
     * @param  int    $id
     * @return A model of the record
     */
    public static function find(int $id)
    {
        $class = static::class;
        $model = new $class;

        return $model->setProperties($id);
    }

    /**
     * Finds a record from the db and sets the values
     * of the column to its respective property
     *
     * @param  int    $id
     * @return A model of the record
     */
    private function setProperties(int $id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE {$this->primaryKey} = {$id} LIMIT 1";

        $stmt = $this->pdo->query($sql);

        if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //store the fields for update reference;
            $this->fields = $row;

            foreach ($row as $k => $v) {
                $this->{$k} = $v;
            }
            return $this;
        }

        return false;
    }

    /**
     * Update the represent record
     */
    public function update()
    {
        $updateString = "";
        $updateData = [];

        foreach ($this->fields as $k => $v) {
            if ($v !== $this->{$k}) {
                $updateString .= ", {$k}=:{$k}";
                //store the value from the model objects
                $updateData[':' . $k] = $this->{$k};
            }
        }

        if ($updateString !== "") {
            $updateString = substr($updateString, 2);
            $updateData[':primarykey'] = $this->{$this->primaryKey};

            $sql = "UPDATE {$this->table} SET {$updateString} WHERE {$this->primaryKey}=:primarykey";

            $stmt = $this->pdo->prepare($sql);

            return $stmt->execute($updateData);
        }

        return false;
    }

    /**
     * Insert a record derived on this model
     * @return [type] [description]
     */
    public function save() {

        $fieldString = "";
        $valueString = "";
        $insertData = [];

        foreach ($this->fields as $k => $v) {

            if (isset($this->{$k}) && $k !== $this->primaryKey) {
                $fieldString .= ", {$k}";
                $valueString .= ", :{$k}";

                $insertData[':' . $k] = $this->{$k};
            }
        }

        if ($fieldString !== "") {
            $fieldString = substr($fieldString, 2);
            $valueString = substr($valueString, 2);

            $sql = "INSERT INTO {$this->table} ({$fieldString}) VALUE ({$valueString});";
            $stmt = $this->pdo->prepare($sql);

            return $stmt->execute($insertData);
        }

        return false;
    }

    /**
     * Deletes record
     * @return [type] [description]
     */
    public function delete() {
        $stmt = $this->pdo->prepare("DELETE FROM {$this->table} WHERE {$this->primaryKey} = :{$this->primaryKey}");
        return $stmt->execute([":{$this->primaryKey}" => $this->{$this->primaryKey}]);
    }

}
