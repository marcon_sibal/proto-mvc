<?php
namespace Msibal\Database;

use PDO;
use PDOException;

/**
 * Database class
 */
class Database
{
    private static $instance;

    public static $pdo;

    private function __construct()
    {
        try {
            $dbDefault = config('db.default');

            $dbConfig = config('db.' . $dbDefault);

            self::$pdo = new PDO(
                $dbDefault . ":host={$dbConfig['host']};dbname={$dbConfig['db']}",
                $dbConfig['username'],
                $dbConfig['password']
            );

            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function __destruct()
    {
        self::$pdo = null;
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Database();
        }

        return self::$instance;
    }

}
